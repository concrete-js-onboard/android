package concrete.com.br.onboardjsandroid.model.api

import java.util.*

/**
 * Created by alex.soares.siqueira on 3/19/2018.
 */

data class User(val _id: Long,
                val name: String,
                val lastName: String,
                val birthdate: Calendar,
                val email: String,
                val phone: Phone,
                val company: String,
                val notes: String)

data class Phone(val ddd: String,
                 val number: String)