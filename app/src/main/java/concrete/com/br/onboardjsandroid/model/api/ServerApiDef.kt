package concrete.com.br.onboardjsandroid.model.api

import io.reactivex.Observable
import retrofit2.http.GET

/**
 * Created by alex.soares.siqueira on 3/19/2018.
 */
interface ServerApiDef {

    @GET("5aafe8512d00004d006f0094")
    fun getUsers(): Observable<User>

}